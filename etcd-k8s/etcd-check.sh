#!/bin/bash

LOCAL_IP=172.16.101.182
ETCD_VERSION=3.4.3-0

docker run --rm -it \
--net host \
-v /etc/kubernetes:/etc/kubernetes k8s.gcr.io/etcd:${ETCD_VERSION} etcdctl \
--cert /etc/kubernetes/pki/etcd/peer.crt \
--key /etc/kubernetes/pki/etcd/peer.key \
--cacert /etc/kubernetes/pki/etcd/ca.crt \
--endpoints https://${LOCAL_IP}:2379 endpoint health --cluster