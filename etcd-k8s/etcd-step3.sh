#!/bin/bash
export HOST0=172.16.101.182
export HOST1=172.16.101.183
export HOST2=172.16.101.184

cp /tmp/${HOST1}/kubeadmcfg.yaml /root/

scp -r /tmp/${HOST1}/pki root@${HOST1}:/etc/kubernetes
scp -r /tmp/${HOST2}/pki root@${HOST2}:/etc/kubernetes
scp -r /tmp/${HOST1}/kubeadmcfg.yaml root@${HOST1}:/root/kubeadmcfg.yaml
scp -r /tmp/${HOST2}/kubeadmcfg.yaml root@${HOST2}:/root/kubeadmcfg.yaml