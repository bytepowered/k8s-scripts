#!/bin/bash
export HOST0=172.16.101.182
export HOST1=172.16.101.183
export HOST2=172.16.101.184

kubeadm init phase etcd local --config=/root/kubeadmcfg.yaml
ssh root@${HOST1} "kubeadm init phase etcd local --config=/root/kubeadmcfg.yaml"
ssh root@${HOST2} "kubeadm init phase etcd local --config=/root/kubeadmcfg.yaml"