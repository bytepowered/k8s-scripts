#!/bin/bash
# Autor: yongjia.chen@hotmail.com
# Github: github.com/yoojiapro

DOCKER_VERSION=20.10.6
KUBE_VERSION=1.18.9
DOCKER_GRAPH_DIR=/data/docker

echo "# 关闭 Firewalld"
systemctl stop firewalld && systemctl disable firewalld

echo "# 关闭 Swap"
swapoff -a && sudo sed -i '/ swap / s/^/#/' /etc/fstab

echo "# 禁用 SELinux"
setenforce 0
sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config

echo "# 桥接IP流量（RHEL/CentOS 7）"
cat > /etc/sysctl.d/k8s.conf << EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sysctl --system

echo "# 启用 br_netfilter 模块"
modprobe br_netfilter

echo "# 启用IPVS内核"
modprobe ip_vs
modprobe ip_vs_rr
modprobe ip_vs_wrr
modprobe ip_vs_sh
modprobe nf_conntrack_ipv4
lsmod | grep ip_vs

cat >/etc/modules-load.d/k8s-ipvs.conf<<EOF
ip_vs
ip_vs_rr
ip_vs_wrr
ip_vs_sh
nf_conntrack_ipv4
EOF

echo "# 添加Kubernetes/Aliyun软件源"

cat << EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64/
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg https://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
EOF

yum update -y && yum install -y yum-utils
yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo

echo "# 配置Docker信息目录: ${DOCKER_GRAPH_DIR}"
mkdir -p /data/docker
cat > /etc/docker/daemon.json << EOF
{
    "graph": "${DOCKER_GRAPH_DIR}",
    "oom-score-adjust":-1000,
    "log-driver":"json-file",
    "log-opts":{
        "max-size":"100m",
        "max-file":"3"
    },
    "max-concurrent-downloads":10,
    "max-concurrent-uploads":10,
    "registry-mirrors":[
        "https://wd9ar864.mirror.aliyuncs.com",
        "http://f1361db2.m.daocloud.io"
    ],
    "insecure-registries":[

    ],
    "storage-driver":"overlay2",
    "storage-opts":[
        "overlay2.override_kernel_check=true"
    ]
}
EOF

echo "# 安装docker，版本: ${DOCKER_VERSION}"
yum install docker-ce-3:${DOCKER_VERSION} -y
systemctl enable docker && systemctl start docker

echo "# 修改Docker CGroupDriver为：systemd"
sed -i 's/--containerd/--exec-opt native.cgroupdriver=systemd --containerd/' /usr/lib/systemd/system/docker.service

echo "# 启动Dockere服务"
systemctl daemon-reload && systemctl restart docker

echo "# 安装Kubenetes核心组件，版本: ${KUBE_VERSION}"
yum install -y kubelet-${KUBE_VERSION} kubeadm-${KUBE_VERSION} kubectl-${KUBE_VERSION}
systemctl enable kubelet && systemctl start kubelet

