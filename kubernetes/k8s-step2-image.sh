#!/bin/bash
# Autor: yongjia.chen@hotmail.com
# Github: github.com/yoojiapro

!/bin/bash
# Autor: yongjia.chen@hotmail.com
# Github: github.com/yoojiapro

COREDNS_VERSION=1.6.7
PAUSE_VERSION=3.2
ETCD_VERSION=3.4.3-0

echo "# 修正CoreDNS image，版本: ${COREDNS_VERSION}"
docker pull coredns/coredns:${COREDNS_VERSION}
docker tag registry.cn-hangzhou.aliyuncs.com/google_containers/coredns:${COREDNS_VERSION} coredns/coredns:${COREDNS_VERSION}

echo "# 修正Pause image，版本: ${PAUSE_VERSION}"
docker pull registry.cn-hangzhou.aliyuncs.com/google_containers/pause:${PAUSE_VERSION}
docker tag registry.cn-hangzhou.aliyuncs.com/google_containers/pause:${PAUSE_VERSION} k8s.gcr.io/pause:${PAUSE_VERSION}

echo "# 修正Etcd image，版本: ${ETCD_VERSION}"
docker pull registry.cn-hangzhou.aliyuncs.com/google_containers/etcd:${ETCD_VERSION}
docker tag registry.cn-hangzhou.aliyuncs.com/google_containers/etcd:${ETCD_VERSION} k8s.gcr.io/etcd:${ETCD_VERSION}

echo "# 拉取kubeadm 依赖的images"
kubeadm config images pull --image-repository=registry.cn-hangzhou.aliyuncs.com/google_containers

